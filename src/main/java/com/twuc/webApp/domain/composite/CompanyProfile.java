package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class CompanyProfile {
    @Id
    @GeneratedValue
    private Long id;
    @Embedded
    private Address address;

    public CompanyProfile() {
    }

    public CompanyProfile(String city, String street) {
        this.address = new Address(city, street);
    }

    public Long getId() {
        return id;
    }

    public String getCity() {
        return address.getCity();
    }

    public String getStreet() {
        return address.getStreet();
    }

    public Address getAddress() {
        return address;
    }
}
