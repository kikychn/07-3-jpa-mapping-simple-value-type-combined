package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class UserProfile {
    @Id
    @GeneratedValue
    private Long id;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "city", column = @Column(name = "address_city")),
            @AttributeOverride(name = "street", column = @Column(name = "address_street"))
    })
    private Address address;

    public UserProfile() {
    }

    public UserProfile(String address_city, String address_street) {
        this.address = new Address(address_city, address_street);
    }

    public Long getId() {
        return id;
    }

    public String getAddressCity() {
        return address.getCity();
    }

    public String getAddressStreet() {
        return address.getStreet();
    }

    public Address getAddress() {
        return address;
    }
}
