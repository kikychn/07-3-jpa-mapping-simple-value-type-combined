package com.twuc.webApp.domain;

import com.twuc.webApp.domain.composite.CompanyProfile;
import com.twuc.webApp.domain.composite.CompanyProfileRepository;
import com.twuc.webApp.domain.composite.UserProfile;
import com.twuc.webApp.domain.composite.UserProfileRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;

//@DataJpaTest(showSql = false)
//@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class SimpleMappingAndValueTypeTest extends JpaTestBase {
    @Autowired
    private CompanyProfileRepository companyProfileRepository;

    @Autowired
    private UserProfileRepository userProfileRepository;

//    @Autowired
//    EntityManager entityManager;

    @Test
    void should_save_and_find_an_CompanyProfile_entity() {
        final CompanyProfile[] savedCompanyProfile = new CompanyProfile[1];
        flushAndClear(entityManager -> {
            savedCompanyProfile[0] = companyProfileRepository
                    .save(new CompanyProfile("Chengdu", "Tianfu 5"));
        });
        CompanyProfile foundCompanyProfile = companyProfileRepository
                .findById(savedCompanyProfile[0].getId()).orElseThrow(NoSuchElementException::new);
        assertEquals(savedCompanyProfile[0].getCity(), foundCompanyProfile.getCity());
        assertEquals(savedCompanyProfile[0].getStreet(), foundCompanyProfile.getStreet());
    }

    @Test
    void should_save_and_find_an_UserProfile_entity() {
        final UserProfile[] savedUserProfile = new UserProfile[1];
        flushAndClear(entityManager -> {
            savedUserProfile[0] = userProfileRepository.save(new UserProfile("Chengdu", "Tianfu 5"));
        });
        UserProfile foundUserProfile = userProfileRepository.findById(savedUserProfile[0].getId()).orElseThrow(NoSuchElementException::new);
        assertEquals(savedUserProfile[0].getAddressCity(), foundUserProfile.getAddressCity());
        assertEquals(savedUserProfile[0].getAddressStreet(), foundUserProfile.getAddressStreet());
    }
}
